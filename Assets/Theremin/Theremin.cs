﻿/**
 * HOW TO USE
 * Put the script on a gameObject
 * Theremin.instance.axis // Vector2 between 0 and 1
**/
using UnityEngine;
using System.Collections;
using System;
public class Theremin : MonoBehaviour
{


	public int pin = 0;
	public int pinValue;


	private GUIStyle currentStyle = null;
	public Color simulateSquareColor = new Color(0f, 1f, 0f, 0.8f);
	public bool m_simulate = true;
	public bool m_simulateMouse = false;
	public bool debugText = true;
	public bool noramlizeOutput = true;
	public bool hideMouse = true;
	public enum Types { Horizontal, Vertical, Both }
	public Types type;

	private float xAxis;
	private float yAxis;

	public void SetRawValueAsPourcent(float valueAsPourcent)
	{
		xAxis =  valueAsPourcent * Screen.width;
		yAxis =  valueAsPourcent * Screen.height;
	}
	public void SetRawValueFrom(float value)
	{
		xAxis = yAxis = value;
	}
	public Vector2 axis
    {
        get
        {
            if (m_simulate && noramlizeOutput)
                return new Vector2(xAxis / Screen.width, yAxis / Screen.height);
            else if (m_simulate)
                return new Vector2(xAxis, yAxis);
            else if (noramlizeOutput)
                return new Vector2(xAxis / Screen.width, yAxis / Screen.height);
            else
                return new Vector2(xAxis, yAxis);
        }
    }
	public static Theremin instance;

	void Start()
	{
		if (instance != null)
			throw new Exception("Only one instance of theremin please !");
		instance = this;
		
	}
    
	protected void Update()
	{
        if (m_simulate)
        {
			if (m_simulateMouse) { 
				Cursor.visible = !hideMouse;
				Vector2 mouse = Input.mousePosition;
				xAxis = type == Types.Horizontal || type == Types.Both ? Mathf.Clamp(mouse.x, 0, Screen.width) : 0;
				yAxis = type == Types.Vertical || type == Types.Both ? Mathf.Clamp(mouse.y, 0, Screen.height) : 0;
			}
        }
	}
	//void OnGUI()
	//{
	//	if (simulate)
	//	{
	//		InitStyles();
	//		GUI.Box(new Rect(xAxis - 10, Screen.height - yAxis - 10, 20, 20), "", currentStyle);
	//	}
	//	if(debugText)
	//	{
	//		GUI.Label(new Rect(0,0,100,20), "("+axis.x+","+axis.y+")");
	//	}
	//}

	private void InitStyles()
	{
		if (currentStyle == null)
		{
			currentStyle = new GUIStyle(GUI.skin.box);
			currentStyle.normal.background = MakeTex(2, 2, simulateSquareColor);
		}
	}

	private Texture2D MakeTex(int width, int height, Color col)
	{
		Color[] pix = new Color[width * height];
		for (int i = 0; i < pix.Length; ++i)
		{
			pix[i] = col;
		}
		Texture2D result = new Texture2D(width, height);
		result.SetPixels(pix);
		result.Apply();
		return result;
	}
}
