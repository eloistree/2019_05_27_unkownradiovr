﻿using UnityEngine;
using System.Collections;

public class labyrinth_victory : MonoBehaviour {
	labyrinth_game game;

	void Start () {
		game = GameObject.FindWithTag("world").GetComponent<labyrinth_game>();
	}

	void OnTriggerEnter(Collider other) {
        if(other.transform.tag == "Player") {
        	game.onVictory();
        }
    }
}
