﻿using UnityEngine;
using System.Collections.Generic;

public class labyrinth_game : MonoBehaviour {
	public GameObject ballPrefab;
	public List<GameObject> levels;
	public labyrinth_camera_controller cameraController;
	
	private GameObject ball;
	private GameObject level;
	private Transform spawn;

	// Use this for initialization
	void Start () {
		InitialiseGame(true);
	}

	void InitialiseGame(bool loadLevel) {
		// initialise level
		if(loadLevel) {
			int randomIdentifier = Random.Range(0, levels.Count);
			GameObject randomLevel = levels[randomIdentifier];

			level = (GameObject)Instantiate(randomLevel, Vector3.zero, Quaternion.identity);
			level.transform.parent = GameObject.FindWithTag("world").transform;

			spawn = GameObject.FindWithTag("spawn").transform;
		}

		// initialise ball
		if(ball) {
			Destroy(ball);
		}

		ball = (GameObject)Instantiate(ballPrefab, spawn.position, Quaternion.identity);
		ball.transform.parent = level.transform;

		cameraController.FollowDaBall(ball.transform);
	}

	public void onFail() {
		Debug.Log("Failed!");
		GameManager.instance.LooseMinigame();
	}

	public void onVictory() {
		Debug.Log("Victory!");
        GameManager.instance.WinMinigame();
	}
}
