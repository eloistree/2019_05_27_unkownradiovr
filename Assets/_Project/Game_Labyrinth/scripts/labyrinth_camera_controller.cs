﻿using UnityEngine;
using System.Collections;

public class labyrinth_camera_controller : MonoBehaviour {

	public Vector3 offset;
	Transform ball;

	private Vector3 targetPosition;
	private Vector3 currentPosition;

	// Use this for initialization
	void Start () {
		currentPosition = targetPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		targetPosition = ball.position;
		currentPosition += (targetPosition - currentPosition) * Time.deltaTime * 2.0f;
		transform.position = currentPosition + offset;
	}

	public void FollowDaBall(Transform daBall) {
		ball = daBall;
	}
}
