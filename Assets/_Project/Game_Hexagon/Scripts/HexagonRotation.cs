﻿using UnityEngine;
using System.Collections;

public class HexagonRotation : MonoBehaviour {

	public float increaseSpeed = 1;
	public float ySpeed;
	private float currentSpeed;

	void Start () {
	
	}
	
	void Update () 
	{
		currentSpeed = Mathf.Lerp(currentSpeed, ySpeed, Time.deltaTime * increaseSpeed);
		transform.Rotate(0, Time.deltaTime * currentSpeed, 0);
		if (Mathf.Abs(Mathf.Abs(currentSpeed) - Mathf.Abs(ySpeed)) < 1)
		{
			currentSpeed = 0;
			ySpeed *= -1;
		}
	}
}
