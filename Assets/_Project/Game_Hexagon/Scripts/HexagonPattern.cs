﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HexagonPattern : MonoBehaviour
{

	public int segmentOffset;
	public float startRadius = 4;
	public float size = 2;
	public float height = 2;
	public int bricksCount = 8;
	public bool removable;
	public Material material;
	public int segmentStep = 0;
	List<HexagoneGenerator1> hexagons;
	public enum Patterns { Loop, Alternate, Staircase, StaircaseReversed, OneFullOneEmpty }
	public Patterns pattern;
	
	void Start()
	{
		switch(pattern)
		{
			case Patterns.Loop: patternLoop(); break;
			case Patterns.Alternate: patternAlternate(); break;
			case Patterns.Staircase: patternStaircase(); break;
			case Patterns.StaircaseReversed: patternStaircase(true); break;
			case Patterns.OneFullOneEmpty: patternOneFullOneEmpty(); break;
		}
	}

	void Update()
	{
		if (pattern == Patterns.Staircase || pattern == Patterns.StaircaseReversed || pattern == Patterns.OneFullOneEmpty)
		{
			int first = -1;
			for (int i = 0; i < hexagons.Count; i++)
			{
				
				if (hexagons[i] == null) continue;
				if (first == -1) first = i;
				if (i != first)
				hexagons[i].radius = hexagons[i-1].radius + size * (segmentStep -1 + (6-(HexagonGlobals.segments-3))/2);
				hexagons[i].segments = HexagonGlobals.segments;
			}
			if (first == -1)
				Destroy(gameObject);
		}
		else
		{
			int ceil = Mathf.Min(hexagons.Count, Mathf.CeilToInt(HexagonGlobals.segments));
			int floor = Mathf.FloorToInt(HexagonGlobals.segments);
			float rest = HexagonGlobals.segments - floor;
			float rest2 = HexagonGlobals.segments % 1;

			for (int i = 0; i < floor; i++)
			{
				if (i >= hexagons.Count || hexagons[i] == null) continue;
				hexagons[i].restPieces = 0;
				hexagons[i].segments = HexagonGlobals.segments;
			}
			for (int i = floor; i < ceil; i++)
			{
				if (i >= hexagons.Count || hexagons[i] == null) continue;
				hexagons[i].restPieces = 1;
				hexagons[i].segments = HexagonGlobals.segments;
			}
		}
	}
	public void patternLoop()
	{
		hexagons = new List<HexagoneGenerator1>();
		for (int i = 0; i < bricksCount; i++)
		{
            HexagoneGenerator1 created = HexagonGlobals.GenerateShape(transform, material, i, startRadius, size, HexagonGlobals.segments, height, removable);
            created.isConvex = true;
            hexagons.Add((created));
           
        }
		
	}
	public void patternAlternate()
	{
		hexagons = new List<HexagoneGenerator1>();
        for (int i = 0; i < bricksCount; i++)
        {
            HexagoneGenerator1 created = HexagonGlobals.GenerateShape(transform, material, i * 2 + segmentOffset, startRadius, size, HexagonGlobals.segments, height, removable);
            created.isConvex = true;
            created.GetComponent<Collider>().enabled = false;
            hexagons.Add((created));
            
        }
        }

	public void patternStaircase(bool reversed = false)
	{
		hexagons = new List<HexagoneGenerator1>();
		float radius = startRadius;
		
		for (int i = 0; i < bricksCount; i++)
		{
            HexagoneGenerator1 created =HexagonGlobals.GenerateShape(transform, material, i, radius, size, HexagonGlobals.segments, height, removable, HexagonGlobals.speed);
            created.isConvex = true;
            hexagons.Insert(reversed ? hexagons.Count : 0 ,created);
			radius += size * segmentStep;
        }
	}


	public void patternOneFullOneEmpty()
	{
		hexagons = new List<HexagoneGenerator1>();
		float radius = startRadius;

		for (int i = 0; i < bricksCount; i++)
        {
            HexagoneGenerator1 created = HexagonGlobals.GenerateShape(transform, material, i * 2, radius, size, HexagonGlobals.segments, height, removable, HexagonGlobals.speed);
              hexagons.Insert(0, (created));
			radius += size * segmentStep;
            created.isConvex = true;
        }
	}
}
