﻿using UnityEngine;
using System.Collections;

public class HexagonTIme : MonoBehaviour {

	public float timeIncrease = .1f;

	void Start()
	{
		Time.timeScale = 1;
	}
	void Update()
	{
		Time.timeScale += timeIncrease * Time.deltaTime;
	}
}
