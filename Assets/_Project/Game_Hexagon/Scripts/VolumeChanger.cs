﻿using UnityEngine;
using System.Collections;

public class VolumeChanger : MonoBehaviour {

	public float targetVolume = 2f;
	void Start () {
        GetComponent<AudioSource>().volume = targetVolume;
	}
	
}
