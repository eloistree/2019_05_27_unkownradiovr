﻿	using UnityEngine;
using System.Collections;
using ProceduralToolkit;

public class TriangleGenerator : MonoBehaviour {

	public float radius = 1f;
	public float height = 1f;

	private MeshDraft draft;
	private MeshFilter filter;

	void Start ()
	{
		filter = GetComponent<MeshFilter>();
            
		draft = MeshE.PrismDraft(radius, 3, height);
		filter.mesh = draft.ToMesh();

	}
	
}
