﻿
using UnityEngine;
using ProceduralToolkit;

    /// <summary>
    /// A simple terrain based on Perlin noise and coloured according to height
    /// </summary>
	[RequireComponent(typeof (MeshRenderer), typeof (MeshFilter))]
	public class HexagoneGenerator : MonoBehaviour {

		public float radius = 10;
		public float size = 10;
        public float height = 10;
		public int segments = 5;
		public int startSegment = 0;

		public float destroyRadius = 1f;
		public float decreaseSpeed = 0.1f;

        private MeshDraft draft;
        private Gradient gradient;
		private MeshFilter filter;

        private void Start()
        {
			filter = GetComponent<MeshFilter>();
            
        }

        private void Update()
        {
			draft = MeshE.Prism2Draft(radius, radius - size, segments, startSegment,  height);
			Mesh m = draft.ToMesh();
			filter.mesh = m;
        if (GetComponent<Collider>() != null && GetComponent<Collider>() is MeshCollider) {
            MeshCollider mc = GetComponent<MeshCollider>() as MeshCollider;
            mc.sharedMesh= m;
           
        }

        radius -= Time.deltaTime * decreaseSpeed;
			
			if (radius < destroyRadius)
				Destroy(gameObject);
        }

      
   
}