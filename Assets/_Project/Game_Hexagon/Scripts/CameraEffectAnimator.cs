﻿using UnityEngine;
using System.Collections;

public class CameraEffectAnimator : MonoBehaviour {

	private TwirlEffect effect;

	void Start () {
		effect = GetComponent<TwirlEffect>();
		iTween.ValueTo(gameObject, iTween.Hash("onupdate", "updateValues", "from", -30, "to", 30, "easetype", iTween.EaseType.easeInOutCirc, "looptype", iTween.LoopType.pingPong, "time", 5f));
	}
	
	// Update is called once per frame
	void updateValues(float v)
	{
		effect.angle = v;
		effect.radius = new Vector2(v/25, v/25);
	}
}
