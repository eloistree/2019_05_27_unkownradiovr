﻿using UnityEngine;
using System.Collections;

public static class IntExtensions
{

	public static int IncrementOrResetIfHigherThan(this int index, int max)
	{
		index++;
		if (index > max)
			index = 0;
		return index;
	}

}
