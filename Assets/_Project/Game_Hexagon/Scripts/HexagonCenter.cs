﻿using UnityEngine;
using System.Collections;
using ProceduralToolkit;

public class HexagonCenter : MonoBehaviour {

	public float radius = 5;
	public float height = 2;
	

	private MeshDraft draft;
	private MeshFilter filter;
	void Start()
	{
		filter = GetComponent<MeshFilter>();
	}
	void Update () 
	{
		
		draft = MeshE.PrismDraft(radius, HexagonPatterns.segments, height);
		filter.mesh = draft.ToMesh();

	}
}
