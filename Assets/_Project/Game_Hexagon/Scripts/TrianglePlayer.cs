﻿using UnityEngine;
using System.Collections;

public class TrianglePlayer : MonoBehaviour {

	// Use this for initialization
	public void Hit () {
        StartCoroutine(Flickr());
	}
	
	// Update is called once per frame
    IEnumerator Flickr()
    {
	    for(int i=0; i < 8;i++)
        {
            GetComponent<Renderer>().material.color = Color.black;
            yield return new WaitForSeconds(0.1f);
            
            GetComponent<Renderer>().material.color = Color.white ;
            yield return new WaitForSeconds(0.1f);
        }
	}
}
