﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class HexagonColorManager : MonoBehaviour {

	[System.Serializable]
	public struct ColorStep
	{
		public Color bricksColor;
		public Color bg1Color;
		public Color bg2Color;
	}
	public Material bricksMaterial;
	public Material bg1Material;
	public Material bg2Material;
	public ColorStep[] steps;
	private int stepIndex;
	public float stepDuration;
	public float tweenDuration;

	IEnumerator Start()
	{

		
		while (true)
		{
			bricksMaterial.DOColor(steps[stepIndex].bricksColor, tweenDuration);
			bg1Material.DOColor(steps[stepIndex].bg1Color, tweenDuration);
			bg2Material.DOColor(steps[stepIndex].bg2Color, tweenDuration);
			yield return new WaitForSeconds(stepDuration);
			stepIndex++;
			if (stepIndex > steps.Length - 1)
				stepIndex = 0;
		}
	}
}
