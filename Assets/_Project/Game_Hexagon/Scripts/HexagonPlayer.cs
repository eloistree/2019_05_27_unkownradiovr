﻿using UnityEngine;
using System.Collections;

public class HexagonPlayer : MonoBehaviour {

	public GameObject prefab;
	public float radisu = 5f;
	public float rotationSpeed = 5f;
	
	void Start () 
	{
		//GenerateShape();
	}
	void Update()
	{
		Vector2 v = Theremin.instance.axis;
		float x = v.x  -0.5f;
		transform.Rotate(0, Time.deltaTime * rotationSpeed * x, 0);

		if (Input.GetKey(KeyCode.LeftArrow))
			transform.Rotate(0, Time.deltaTime * -rotationSpeed, 0);
		if (Input.GetKey(KeyCode.RightArrow))
			transform.Rotate(0, Time.deltaTime * rotationSpeed, 0);
	}
	private void GenerateShape( )
	{
		GameObject inst = (GameObject)Instantiate(prefab);
		inst.transform.parent = transform;
		inst.transform.localPosition += transform.forward * radisu;
	}
}
