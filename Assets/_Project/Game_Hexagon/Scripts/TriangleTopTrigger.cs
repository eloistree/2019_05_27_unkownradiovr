﻿using UnityEngine;
using System.Collections;

public class TriangleTopTrigger : MonoBehaviour {

	public AudioClip hitSound;
    public TrianglePlayer triangle;
	public float timeBetweenTwoHits = 0.5f;
	private float lastHit = 0;

	
	// Update is called once per frame
	void OnTriggerEnter ( Collider c ) 
	{
		if (lastHit > Time.time - timeBetweenTwoHits)
			return;

		lastHit = Time.time;

        GetComponent<AudioSource>().clip = hitSound;
        GetComponent<AudioSource>().Play();
		HexagonLife.instance.Hit();
        triangle.Hit();
		//Destroy(c);
	}
}
