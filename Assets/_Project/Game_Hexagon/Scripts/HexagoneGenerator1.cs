﻿
using UnityEngine;
using ProceduralToolkit;

	[RequireComponent(typeof (MeshRenderer), typeof (MeshFilter))]
	public class HexagoneGenerator1 : MonoBehaviour {

		public float radius = 10;
		public float size = 10;
        public float height = 10;
		public float segments = 5f;
		public int startSegment = 0;

		public float destroyRadius = 1f;
		public float decreaseSpeed = 0;
		public int restPieces;
        private MeshDraft draft;
        private Gradient gradient;
		private MeshFilter filter;
		public bool removable;
    public MeshCollider mc; 
    public bool isConvex;
        private void Start()
        {
			filter = GetComponent<MeshFilter>();
         mc = GetComponent<Collider>() as MeshCollider;
        //mc.convex = true;
    }

		Mesh m;
        private void Update()
        {
        if( mc==null)
            mc = GetComponent<Collider>() as MeshCollider;
        if (mc == null)
            return;
        if (isConvex != mc.convex)
            mc.convex = isConvex;

			draft = MeshE.Prism2Draft1(radius, radius - size, segments, startSegment,  height, restPieces);
			
			if (draft != null)
			{
				Destroy(m);
				m = null;
				Destroy(filter.mesh);
				m = draft.ToMesh();
				

				filter.mesh = m;
            if (GetComponent<Collider>() != null && GetComponent<Collider>() is MeshCollider) {
                MeshCollider mc = GetComponent<Collider>() as MeshCollider;
                mc.sharedMesh = m;
               // mc.convex=true;
            }
            if (GetComponent<Renderer>().enabled == false)
					GetComponent<Renderer>().enabled = true;
			}
			else GetComponent<Renderer>().enabled = false;

			radius -= Time.deltaTime * decreaseSpeed * .01f; 
			if (removable && radius < destroyRadius)
				Destroy(gameObject);
        }

      
   
}