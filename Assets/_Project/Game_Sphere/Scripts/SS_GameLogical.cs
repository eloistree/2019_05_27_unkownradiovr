﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SS_GameLogical : MonoBehaviour {

    public float timer = 60.0f;
    public Text timerText;
    public GameObject GameOverPanel;
    public Text gameOverText;
    public bool isOver;

	// Use this for initialization
	void Start () 
    {
		//GameOverPanel.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (isOver)
        {
            return;
        }

        if (timer > 0.0f )
        {
            timer -= Time.deltaTime;
            if (timerText != null)
            timerText.text = string.Format("Timer: {0}s", timer.ToString("0.00"));
        }

        if (timer <= 0.0f)
        {
            if (timerText != null)
                timerText.text = string.Format("Time Out!", timer.ToString());
            Lose();           
        }
	}

    private void Lose()
    {
        if (gameOverText != null)
            gameOverText.text = "You Lose!";      
        GameOver();
        GameManager.instance.LooseMinigame();
    }

    private void GameOver()
    {
        if (GameObject.Find("First Person Controller"))
        {
			//GameObject.Find("First Person Controller").SetActive(false);
        }
        
       // GameObject.Find("MapCamera").SetActive(false);
		//GameOverPanel.SetActive(true);
		//gameOverText.GetComponent<Animator>().SetTrigger("GameOver");
        isOver = true;
    }

    public void Win()
    {
        if (gameOverText != null)
            gameOverText.text = "You Win!";
        GameOver();
        GameManager.instance.WinMinigame();
    }
}
