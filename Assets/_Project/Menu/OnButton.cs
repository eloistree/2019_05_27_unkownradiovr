﻿using UnityEngine;
using System.Collections;

public class OnButton : MonoBehaviour {

	private bool clicked;
	void OnClick()
	{
		if (clicked)
			return;

		clicked = true;
		GameManager.instance.DisplayInterlude();
	}
	void Update()
	{
		if (Theremin.instance.m_simulate == false && Theremin.instance.axis.x >= 0.8f)
			OnClick();
	}
	
}
