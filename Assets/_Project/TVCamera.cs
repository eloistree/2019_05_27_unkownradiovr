﻿
using UnityEngine;

public class TVCamera : MonoBehaviour
{

    public Camera m_selection;
    public static Camera m_selectionInScene;
    public static Camera GetCamera() { return m_selectionInScene; }

    private void Awake()
    {
        m_selectionInScene = m_selection;
    }
}