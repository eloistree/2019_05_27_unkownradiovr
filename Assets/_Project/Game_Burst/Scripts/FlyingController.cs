﻿using UnityEngine;
using System.Collections;
using ARM;

public class FlyingController : MonoBehaviour {

    public Transform StartingPoint;
    public Transform PoolContainer;

    [SerializeField] float ZSpeed = 5f;
    [SerializeField] float MaxZSpeed = 100f;
    [SerializeField] float rotationSpeed = 360f;

    [HideInInspector]
    public bool isAlive = true;
    [Header("Debug")]
    public float m_rotationValue;
    
   // private SENaturalBloomAndDirtyLens SEN;
    private float oldSENIntensity;

    void Awake()
    {
        GetComponent<Camera>().transform.position = StartingPoint.position;
        GetComponent<Camera>().transform.rotation = StartingPoint.rotation;
     //   SEN = GetComponent<SENaturalBloomAndDirtyLens>();
      //  oldSENIntensity = SEN.bloomIntensity;
    }
	
    void Start()
    {
        isAlive = true;
       // SEN.bloomIntensity = oldSENIntensity;
    }

    void Update () {
        if (isAlive)
        {
            m_rotationValue = (Theremin.instance.axis.x - 0.5f) * rotationSpeed;
            float Zspeed = (GetComponent<Rigidbody>().velocity.z > MaxZSpeed) ? 0 : ZSpeed * Time.deltaTime;
            PoolContainer.Rotate(new Vector3(0, 0, m_rotationValue * Time.deltaTime));
            GetComponent<Rigidbody>().AddForce(0, 0, Zspeed, ForceMode.VelocityChange);
        }
        else
        {
         //   SEN.bloomIntensity = Mathf.Lerp(SEN.bloomIntensity,0.4f, Time.deltaTime * 2f);
        }
	}

    void OnCollisionEnter(Collision c)
    {
        if(c.gameObject.layer == LayerMask.NameToLayer("Killing"))
        {
            MultiplePooled.GetPooledObjectByName("Explosions", transform);
            GetComponent<Rigidbody>().velocity /= 2;
            GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-2f, 2f), Random.Range(0f, 2f), Random.Range(-5f, 0f)), ForceMode.Impulse);
            GameObject.Destroy(c.gameObject);
            isAlive = false;
            Invoke("GameOver", 5.0f);
        }
    }

    public void GameOver()
    {
        GameManager.instance.LooseMinigame();
    }
}
