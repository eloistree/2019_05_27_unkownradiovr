﻿using UnityEngine;
using System.Collections;

public class StartMusicAt : MonoBehaviour {
    
    public float MusicStartAt = 65.5f;

    void Start()
    {
        GetComponent<AudioSource>().time = MusicStartAt;
        GetComponent<AudioSource>().Play();
    }
}
