﻿using UnityEngine;
using System.Collections;

public class EnableAtStart : MonoBehaviour {

    public GameObject toEnableAtStart;
	void Start () {
        toEnableAtStart.SetActive(true);
	}
}
