﻿using UnityEngine;
using System.Collections;

public class TranslateOffset : MonoBehaviour {
    public enum UVScrollingDirection { U, V, UV }

    public float scrollSpeed = 0.5f;
    public UVScrollingDirection direction;
	
    public void Update() { 

        var offset = Time.time * scrollSpeed;

        switch(direction)
        {
            case UVScrollingDirection.U:
                GetComponent<Renderer>().material.mainTextureOffset = new Vector2(offset, 0);
                break;
            case UVScrollingDirection.V:
                GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0, offset);
                break;
            case UVScrollingDirection.UV:
                GetComponent<Renderer>().material.mainTextureOffset = new Vector2(offset, offset);
                break;
        }
    }
}
