﻿using UnityEngine;
using System.Collections;

public class TurnAround : MonoBehaviour {


	public float wantedspeed;
	public float m_speedAdding;
	public Transform m_toAffect;
	// Use this for initialization
	void Start () {
		SphereCollider[] temp = GetComponents<SphereCollider>();
		temp[1].enabled = false;

	}
	
	// Update is called once per frame
	void Update () {
		wantedspeed += m_speedAdding * Time.deltaTime;
		m_toAffect.Rotate(m_toAffect.up, wantedspeed * Time.deltaTime);
//		GetComponent<Rigidbody>().AddTorque(0,(wantedspeed-GetComponent<Rigidbody>().angularVelocity.y)*Time.deltaTime,0,ForceMode.VelocityChange);

		//GetComponent<Rigidbody>().AddForce(new Vector3(,0,),ForceMode.VelocityChange);
		//Debug.Log(GetComponent<Rigidbody>().angularVelocity);
	}
}
