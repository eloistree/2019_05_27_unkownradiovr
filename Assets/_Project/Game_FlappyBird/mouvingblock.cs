﻿using UnityEngine;
using System.Collections;

public class mouvingblock : MonoBehaviour {


	public float speed=2f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(!gamemanager.singleton.gameover)
			transform.Translate(speed*Vector3.left*Time.deltaTime);
		if(transform.position.x < -50f)
			Destroy(this.gameObject);
	}

	public void generatecrate(float spaceavailable){
		float taille=Random.Range(2f,4f);
		GameObject temp= Instantiate(gamemanager.singleton.crate,new Vector3(transform.position.x-0.5f-taille/2f,taille/2f-5f,10),Quaternion.identity) as GameObject;
		temp.transform.localScale = taille*Vector3.one;
		temp.GetComponent<mouvingblock>().speed=this.speed;
	}
}
