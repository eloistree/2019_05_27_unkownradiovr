﻿using UnityEngine;
using System.Collections;

public class gamemanager : MonoBehaviour {


	public bool gameover=false;

	public static gamemanager singleton;
	public GameObject crate;
	public GameObject player;
	// Use this for initialization
	void Start () {
		singleton=this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	//void OnGUI(){
	//	if(gameover){
	//	//	GUI.Label(new Rect(Screen.width/2-100,Screen.height/2-20,200,20),"YOU LOSE");
	//	}

	//}

	public void lose(){
		gameover=true;
		player.GetComponent<Rigidbody>().constraints=RigidbodyConstraints.FreezeAll;
		GameManager.instance.LooseMinigame();

	}
}
