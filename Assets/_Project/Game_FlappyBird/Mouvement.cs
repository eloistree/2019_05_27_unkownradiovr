﻿using UnityEngine;
using System.Collections;
using System;

public class Mouvement : MonoBehaviour {

	public Transform m_bot;
	public Transform m_top;
	public Transform m_toAffect;
	public float m_lerp=2f;
	private float m_current;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

		m_current = Mathf.Lerp(m_current, Theremin.instance.axis.x, Time.deltaTime * m_lerp);

		m_toAffect.position = Vector3.Lerp(m_bot.position, m_top.position, m_current);
	}
}
