﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(Camera))]
public class MoveCameraOnFocus : MonoBehaviour
{

#pragma warning disable 618
    public Camera targetCamera;
     public float minDistanceToChange = 1f;
     public Vector3 lastPosition;
     
	void Start () {
        if( targetCamera==null)
        targetCamera = FindObjectOfType<Camera>() as Camera;
	}
	
	void FixedUpdate () {
        
        if (CameraFocus.Instance == null ) return;
        CameraFocusElement focus = CameraFocus.Instance.currentFocus;
        if (focus == null) return;

        Vector3 newPosition =focus.GetCameraPosition();
        newPosition.z = transform.position.z;
        //float distOldToNew = Vector3.Distance(newPosition, lastPosition);

        float d = Vector3.Distance(newPosition, lastPosition);
        bool useNewOne = d > minDistanceToChange;
            
            targetCamera.transform.position = Vector3.Lerp(targetCamera.transform.position, useNewOne?newPosition:lastPosition, Time.fixedDeltaTime);
        
        targetCamera.orthographicSize = Mathf.Lerp(targetCamera.orthographicSize, focus.GetCameraSize(), Time.fixedDeltaTime);

        if (useNewOne) lastPosition = newPosition; 
	}
}
