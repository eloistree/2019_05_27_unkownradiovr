﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Rigidbody2D))]
public class ColorGame_PlayerMove : MonoBehaviour {


    public Checker wallRightChecker;
    public Checker wallLeftChecker;
    public float forwardPower = 1f;
    public bool goRight = true;

    public void OnEnable()
    {
        Player.onPlayerLifeChange += AutoDestruction;
    }
    public void OnDisable()
    {
        Player.onPlayerLifeChange -= AutoDestruction;
    }


    public void AutoDestruction(int oldLife, int newLife) { Destroy(this); }
    public void Update() {


        if (!wallRightChecker|| !wallLeftChecker) return;

        if (countDownInverse > 0) countDownInverse -= Time.deltaTime;
        if ((goRight && wallRightChecker.IsColliding2D()) || (!goRight && wallLeftChecker.IsColliding2D())) 
        {
            Inverse();
        }

        Vector2 velocity = GetComponent<Rigidbody2D>().velocity;
        velocity.x=(goRight?1f:-1f)*forwardPower;
        GetComponent<Rigidbody2D>().velocity = velocity;

    
    }
    private float countDownInverse;
    private void Inverse()
    {
        if (countDownInverse >0)return;
        goRight = !goRight;
        countDownInverse = 0.3f ;
    }


}
