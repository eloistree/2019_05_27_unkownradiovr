﻿using UnityEngine;
using System.Collections;

public class DisplayOnTrigger : MonoBehaviour {

    public GameObject target;
    public bool isActivateAtStart;
    public bool deactivateAtTriggerEnd=true;

    
    void Start () {
        if (!target){ Destroy(this);return;}
        target.SetActive(isActivateAtStart);
	}



    public void OnTriggerEnter2D(Collider2D col)
    {
        if (!col.gameObject.tag.Equals("Player")) return;

        if(target)
        target.SetActive(true);

    }

    public void OnTriggerExit2D(Collider2D col)
    {
        if (!col.gameObject.tag.Equals("Player")) return;

        if(deactivateAtTriggerEnd)
            target.SetActive(false);

    }
}
