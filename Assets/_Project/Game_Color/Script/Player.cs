﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    private static int _life = 1;

    public static int Life
    {
        get { return Player._life; }
        set {
            if (_life <= 0 && value <= 0) return;
            if (onPlayerLifeChange != null) 
                onPlayerLifeChange(_life, value);
            if(value<=0)
                if(onPlayerDeath!=null)
            onPlayerDeath();
            Player._life = value; 
            
        }
    }

    public delegate void OnPlayerDeath();
    public static OnPlayerDeath onPlayerDeath;
    public delegate void OnPlayerLifeChange(int oldLife,int newLife);
    public static OnPlayerLifeChange onPlayerLifeChange;
}
