﻿using UnityEngine;
using System.Collections;

public class MaxTimeLevel : MonoBehaviour {

    public float maxTimeInTheLevel = 200f;
    public float countDown;
    void OnEnable() 
    {
        countDown = maxTimeInTheLevel;

    
    }
	// Update is called once per frame
	void Update () {
        countDown -= Time.deltaTime;
        if (countDown < 0) {

            if (GameManager.instance != null)
                GameManager.instance.LooseMinigame();
            else { Debug.LogWarning("Not game manager in the scene"); Application.LoadLevel(Application.loadedLevel); }
        }
	}
}
