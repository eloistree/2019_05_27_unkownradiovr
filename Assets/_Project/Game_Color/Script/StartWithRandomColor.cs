﻿using UnityEngine;
using System.Collections;
 namespace unknowradio.colorgame{
public class StartWithRandomColor : MonoBehaviour {

    public SpriteColorManager spriteColor;
    public float rangeStart=0f;
    public float rangeEnd=1f;
    
    // Use this for initialization
	void Start () {

        if (spriteColor) {
            Color c = new Color();
                c = ColorsGetter.GetLinkedColor(Random.Range(rangeStart, rangeEnd), GameColorManaged.Colors);
            
            spriteColor.ChangeColor(c,true);
        }
        Destroy(this);
    }

	
}
}