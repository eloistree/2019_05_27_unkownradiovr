﻿using UnityEngine;
using System.Collections;
using System;

public class LoadNextLevelIfEndDetected : MonoBehaviour {

	void Start () {

      

	}

    public void LoadNextLevel(string sceneName, bool win, float timeToLoad) 
    {

        StartCoroutine( LoadNextLevel(sceneName, timeToLoad) );
    
    
    }

    public IEnumerator LoadNextLevel(string name, float timeBeforeTakingEffect)
    {
        Debug.Log("Load next level:" + name);
        yield return new WaitForSeconds(timeBeforeTakingEffect);
        try
        {
            Application.LoadLevel(name);
        }
        catch (Exception)
        {
            Debug.Log("Impossible to load level:" + name);
            Application.LoadLevel(Application.loadedLevelName);
            

        }
    }
    void OnDisable() 
    {

        ColorLevelState.onLevelWin -= LoadNextLevel;
    }   void OnEnable() 
    {
        ColorLevelState.onLevelWin += LoadNextLevel;
	
    } 
}
