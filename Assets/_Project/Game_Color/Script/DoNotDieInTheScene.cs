﻿using UnityEngine;
using System.Collections;

public class DoNotDieInTheScene : MonoBehaviour {

    private static DoNotDieInTheScene _instance;

    public static DoNotDieInTheScene Instance
    {
        get { return DoNotDieInTheScene._instance; }
        set {
            if (_instance) { Destroy(value.gameObject); return; }
            DoNotDieInTheScene._instance = value; }
    }


    public string sceneName = "";
    void Awake() {

        DontDestroyOnLoad(this.gameObject);
        Instance = this;
    
    }
    void OnLevelWasLoaded(int level)
    {
        if (! Application.loadedLevelName.Equals(sceneName)) 
        {
            Destroy(gameObject);
            return;
        }

    }
	// Update is called once per frame
	void Update () {
	
	}
}
