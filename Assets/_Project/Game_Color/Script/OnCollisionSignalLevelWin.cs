﻿using UnityEngine;
using System.Collections;

public class OnCollisionSignalLevelWin : MonoBehaviour {


    public bool onEnterTrigger=true;
    public bool onEnterCollision;

    public bool win;
    public string sceneToLoad;



    public void OnCollisionEnter2D(Collision2D col)
    {

        if (onEnterCollision && col.gameObject.tag.Equals("Player"))
            NotifyEndLevel();


    }
    public void OnTriggerEnter2D(Collider2D col)
    {

        if(onEnterTrigger && col.gameObject.tag.Equals("Player"))
            NotifyEndLevel();
    }

    public void OnCollisionEnter(Collision col)
    {

        if (onEnterCollision && col.gameObject.tag.Equals("Player"))
            NotifyEndLevel();


    }
    public void OnTriggerEnter(Collider col)
    {

        if (onEnterTrigger && col.gameObject.tag.Equals("Player"))
            NotifyEndLevel();
    }

    private void NotifyEndLevel()
    {

        //ColorLevelState.EndOfLevelDetected(win, sceneToLoad);

        if (GameManager.instance != null)
            GameManager.instance.DisplayInterlude();
        else { Debug.LogWarning("Not game manager in the scene"); Application.LoadLevel(Application.loadedLevel); }
        Destroy(this);
    }
	
}
