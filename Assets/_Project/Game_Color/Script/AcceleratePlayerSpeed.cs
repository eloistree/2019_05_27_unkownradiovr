﻿using UnityEngine;
using System.Collections;

public class AcceleratePlayerSpeed : MonoBehaviour {

    public float multipliBy=1.4f;
    public void OnTriggerEnter2D(Collider2D col)
    {

        if (!col.gameObject.tag.Equals("Player")) return;


        ColorGame_PlayerMove move = col.gameObject.GetComponent<ColorGame_PlayerMove>() as ColorGame_PlayerMove;
        if (move == null) return;
        move.forwardPower *= multipliBy;
        Destroy(this.gameObject);

        
    }
}
