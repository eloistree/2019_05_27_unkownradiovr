﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class MoveElectricityRandomlyOnLine : MonoBehaviour
{
    public ElectricityFromTo m_electricity;
    public Transform m_start;
    public Transform m_end;
    public float m_timeBetweenChange;
    private IEnumerator Start()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForSeconds(m_timeBetweenChange);
            m_electricity.SetStartPoint(GetRandomPointBetween(m_start.position, m_end.position));
        }
        
    }

    private Vector3 GetRandomPointBetween(Vector3 position1, Vector3 position2)
    {
        return Vector3.Lerp(position1, position2, UnityEngine.Random.value);
    }

}
