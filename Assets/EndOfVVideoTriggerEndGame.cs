﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class EndOfVVideoTriggerEndGame : MonoBehaviour
{
    public VideoPlayer m_player;
    public float m_timeOfVideo = 20;
    public IEnumerator Start()
    {
        m_player.Play();
        yield return new WaitForSeconds(m_timeOfVideo);
        if (GameManager.instance)
            GameManager.instance.WinMinigame();
    }
  
}
