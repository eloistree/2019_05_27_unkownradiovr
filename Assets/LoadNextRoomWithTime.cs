﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadNextRoomWithTime : MonoBehaviour
{
    public float m_timeToLoadnextRoom=3;
    void Start()
    {
        Invoke("GoNextGame", m_timeToLoadnextRoom);
    }
    public void GoNextGame() {
        GameManager.instance.WinMinigame();
    }

}
