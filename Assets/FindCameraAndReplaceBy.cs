﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class FindCameraAndReplaceBy : MonoBehaviour
{
    public RenderTexture m_renderTexture;
    
    private void OnLevelWasLoaded(int level)
    {
        if(TVCamera.GetCamera() != null)
            TVCamera.GetCamera().targetTexture = m_renderTexture;
    }
}

