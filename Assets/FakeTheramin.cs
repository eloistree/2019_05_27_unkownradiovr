﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeTheramin : MonoBehaviour
{
    public Transform m_antennaPoint;
    public Transform [] m_trackedPoints;
    public float m_effectiveDistance=1;
    public float m_safeDistance = 0.2f;
     void Update()
    {
    
        if (m_trackedPoints.Length == 0) return;
        else if (m_trackedPoints.Length > 0 ) {

            float distMin = 100000;
            for (int i = 0; i < m_trackedPoints.Length; i++)
            {

                float d =  Vector3.Distance(RemoveY(m_trackedPoints[i].position), RemoveY(m_antennaPoint.position));
                if (d < distMin)
                    distMin = d;
            }
            if (Theremin.instance) {

                Theremin.instance.m_simulate = false;
                Theremin.instance.SetRawValueAsPourcent (
                    Mathf.Clamp((Mathf.Abs(distMin)- (m_safeDistance))/( m_effectiveDistance- (m_safeDistance)),0,1f));
            }
        }

    }

    private Vector3 RemoveY(Vector3 position)
    {
        position.y = 0;
        return position;
    }
}
