﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricityFromTo : MonoBehaviour
{
    public Transform m_whatToMove;
    public Transform m_startPoint;
    public Transform m_endPoint;
    public Transform m_lookUpDirection;
    public Vector3 m_rotationAdjustement;
    public float m_radius=0.1f;

    void LateUpdate()
    {
        Vector3 direction = (m_endPoint.position - m_startPoint.position);
        m_whatToMove.LookAt(m_endPoint, m_lookUpDirection.up);
      //  m_whatToMove.Rotate(m_rotationAdjustement, Space.Self);
        m_whatToMove.up= direction;
        m_whatToMove.position = m_startPoint.position + (direction * 0.5f);
        Transform parent = m_whatToMove.parent;
        m_whatToMove.parent = null;
        m_whatToMove.localScale = new Vector3(m_radius, direction.magnitude, m_radius);
        m_whatToMove.parent = parent;
        Debug.DrawLine(m_endPoint.position, m_startPoint.position, Color.red,0.1f);
    }

    public void SetStartPoint(Vector3 start)
    {
        m_startPoint.position = start;
    }
    public void SetEndPoint(Vector3 end)
    {
        m_endPoint.position = end;
    }

    public void SetPoints(Vector3 start, Vector3 end) {
        SetStartPoint(start);
        SetEndPoint(end);
    }


}
