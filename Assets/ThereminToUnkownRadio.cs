﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThereminToUnkownRadio : MonoBehaviour
{
    public float m_distanceFound;
    public float m_pourcent;
    public ThereminRadioPitchVolumeDemo m_theremin;
    public Theremin m_thereminMono;
    public float m_ratioMinDistance = 1f;
    public float m_ratioMaxDistance = 2f;

    void Update()
    {
        m_distanceFound = m_theremin.GetPitchDistance();
        if (Theremin.instance)
        {
            m_thereminMono = Theremin.instance;
                
            m_pourcent = Mathf.Clamp((m_distanceFound - m_ratioMinDistance) / (m_ratioMaxDistance - m_ratioMinDistance), 0f, 1f);
          Theremin.instance.SetRawValueAsPourcent (m_pourcent);
                
        }

    }
}
